#!/bin/bash

usage()
{
	echo "Usage: $0 <path/to/local.php> <path/to/backup_directory>"
	echo "    -h : This message"
	echo "    -c : Compact mode (extended inserts)"
}

while getopts ":hvc" optname
do
	case "$optname" in
		"h")
		usage;;
	"v")
		VERBOSE=1
		;;
	"c")
		export COMPACT="yes"
		;;
	"?")
		echo "Argument inconnu : -$OPTARG"
		usage;;
	":")
		usage;;
	*)
		echo "Problème inconnu : -$OPTARG"
		usage;;
	esac
	shift $((OPTIND-1))
	OPTIND=1
done

echo "== $1"
if [[ -f "$1" ]]
then
	DB_DETECTED=$(grep "^\$dbs_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	USER_DETECTED=$(grep "^\$user_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	PASS_DETECTED=$(grep "^\$pass_tiki" "$1" | cut -d"'" -f2| tail -n 1);
	CHARSET_DETECTED=$(grep "^\$client_charset" "$1" | cut -d"'" -f2| tail -n 1);
else
	echo "ERROR: local.php file not found"
	usage
	exit 1
fi

if [[ -d "$2" ]]
then
	echo "→ $2"
else
	echo "ERROR: backup directory not found"
	usage
	exit 3
fi

if [[ "$COMPACT" = "yes" ]]
then
	INSERTS="--extended-insert"
	echo "Using ${INSERTS} (compact)"
else
	INSERTS="--skip-extended-insert"
	echo "Using ${INSERTS}"
fi


DATE=$(date +"%F_%T")
DUMP="$2/"dump_db_${DB_DETECTED}_${DATE}.sql
DUMPLOG="$2/"dump_last_sql_${DB_DETECTED}.log
echo $DATE
echo $DUMP

read previouslog < $DUMPLOG
echo "previous: " $previouslog
touch $DUMP || exit 2
mysqldump -u "${USER_DETECTED}" --password="${PASS_DETECTED}" --default-character-set="${CHARSET_DETECTED}" -Qqf ${INSERTS} "${DB_DETECTED}" > $DUMP
echo $DUMP > $DUMPLOG

if [[ -f "$previouslog" ]]
then
	echo "Affiche la différence par rapport à $previouslog ? (y/N)"
	read affiche
	if [ "$affiche" = "y" ]
	then
		diff $previouslog $DUMP | grep -v 'INSERT INTO `tiki_pages`' | grep -v 'INSERT INTO `index_' | head -n 200
	fi
fi
