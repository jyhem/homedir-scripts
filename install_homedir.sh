#/bin/bash

mkdir tools
cd tools/
git clone https://gitlab.com/jyhem/homedir-scripts.git
git clone https://gitlab.com/jyhem/tiki-scripts.git
git clone https://gitlab.com/jyhem/linux-configs.git
cd linux-configs/
cp .screenrc .vimrc .tmux.conf .bash_aliases ~/
cd
mkdir bin
cp tools/tiki-scripts/jml_* bin

echo ". .bash_aliases"
