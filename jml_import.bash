#!/bin/bash

usage()
{
  echo "Usage: $0 <path/to/local.php> <sql file>"
}

echo "== $1"
if [[ -f "$1" ]]
then
  DB_DETECTED=$(grep "^\$dbs_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  USER_DETECTED=$(grep "^\$user_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  PASS_DETECTED=$(grep "^\$pass_tiki" "$1" | cut -d"'" -f2| tail -n 1);
  CHARSET_DETECTED=$(grep "^\$client_charset" "$1" | cut -d"'" -f2| tail -n 1);
else
  echo "ERROR: local.php file not found"
  usage
  exit 1
fi

DATE=$(date +"%F_%T")
IMPORT="$2"
echo $DATE
echo $IMPORT

if [ ! -f "$IMPORT" ] ; then echo "ERROR: imported database not found"; usage; exit 2; else echo "imported database file found"; fi
#mysqldump -u "${USER_DETECTED}" --password="${PASS_DETECTED}" -Qqf --skip-extended-insert "${DB_DETECTED}" > $DUMP
#mysql --force -u "${USER_DETECTED}" --password="${PASS_DETECTED}" --default-character-set=utf8 "${DB_DETECTED}" < "$IMPORT"
mysql --force -u "${USER_DETECTED}" --password="${PASS_DETECTED}" --default-character-set="${CHARSET_DETECTED}" "${DB_DETECTED}" < "$IMPORT"
